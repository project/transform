(function($) {
  Drupal.behaviors.transform = function (context) {
    // Apply transformation if "Enable tranformation" is set
    if (Drupal.settings.transform.enabled) {
      formclasslist = Drupal.settings.transform.formclasslist;
      classes = formclasslist.split('\n');
      $.each(classes, function(){
        $("form"+this).jqTransform();
        // Prevents gripper from be applied to the textarea
        $("form"+this+" textarea").addClass('textarea-processed');
      });
    }
  }
})(jQuery);




